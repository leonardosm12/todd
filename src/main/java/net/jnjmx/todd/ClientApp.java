package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientApp {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		try {
			String server = "192.168.33.11";
			
			if (args.length >= 1) {
				server = args[0];
			}					
			Client c=new Client(server);
			System.out.println("The current time of day on todd server is: "+c.timeOfDay());
			c.close();
			System.out.println("Exiting...");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.exit(2);
		}
	}

}